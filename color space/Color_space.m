
clear all;

close all;
clc;

Irgb = im2double(imread('cell.png'));

% HSV
Ihsv = rgb2hsv(Irgb);
% L*a*b
Ilab = rgb2lab(Irgb);
% Intensity
Iintensity = rgb2gray(Irgb);
% Max value
Imax = max(Irgb, [], 3);

Iintensity_average = sum(sum(Iintensity)) / (size(Iintensity,1) * size(Iintensity, 2))
Imax_average = sum(sum(Imax)) / (size(Imax, 1) * size(Imax, 2))
%fig 1
figure;
subplot(2,2,1);
imagesc(Irgb);
title('RGB image'); axis image; axis off;
subplot(2,2,2);
colormap('hsv');
imagesc(Ihsv(:,:,1));
colorbar;
title('HSV hue'); axis image; axis off;
subplot(2,2,3);
colormap('hsv');
imagesc(Ihsv(:,:,2));
colorbar;
title('HSV saturation'); axis image; axis off;
subplot(2,2,4);
colormap('hsv');
imagesc(Ihsv(:,:,3));
colorbar;
title('HSV value'); axis image; axis off;
% 2
figure;
colormap('hsv');
subplot(2,2,1);
imagesc(Ilab(:,:,1));
colorbar;
title('L*a*b luminance'); axis image; axis off;
subplot(2,2,2);
imagesc(Iintensity);
colorbar;
title('Intensity'); axis image; axis off;
subplot(2,2,3);
imagesc(Ihsv(:,:,3));
colorbar;
title('HSV value'); axis image; axis off; 
subplot(2,2,4);
imagesc(Imax);
colorbar;
title('Max value'); axis image; axis off;


%% 
% close all; 
% clc;
% 3
figure;
colormap('jet');
for i=1:1:12
    subplot(3,4,i);
    labels = ccl(Irgb(:,:,:), 0.13 - 0.01*i, 100);
    imagesc(labels);
    title(0.13 - 0.01*i); 
    axis image; axis off;
end

I_ccl = ccl(Irgb(:,:,:), 0.05, 100);
Ilab_ccl = ccl(Ilab(:,:,:), 5, 100);
Iluminance_ccl = ccl(Ilab(:,:,1), 1.5, 100);
Ichrominance_ccl = ccl(Ilab(:,:,2:3), 3, 100);
% 4
figure;
colormap('jet');
subplot(2,2,1);
imagesc(I_ccl);
title('RGB'); axis image; axis off;
subplot(2,2,2);
imagesc(Ilab_ccl);
title('L*a*b'); axis image; axis off;
subplot(2,2,3);
imagesc(Iluminance_ccl);
title('L*a*b luminance'); axis image; axis off;
subplot(2,2,4);
imagesc(Ichrominance_ccl);
title('L*a*b chrominance'); axis image; axis off;


%% 
% close all;
% clc;

I_kmeans = color_kmeans(Irgb, 4);
Ilab_kmeans = color_kmeans(Ilab, 4);
Iluminance_kmeans = color_kmeans(Ilab(:,:,1), 4);
Ichrominance_kmeans = color_kmeans(Ilab(:,:,2:3), 4);
% 5
figure;
colormap('jet');
subplot(2,2,1);
imagesc(I_kmeans);
title('RGB'); axis image; axis off;
subplot(2,2,2);
imagesc(Ilab_kmeans);
title('L*a*b'); axis image; axis off;
subplot(2,2,3);
imagesc(Iluminance_kmeans);
title('L*a*b luminance'); axis image; axis off;
subplot(2,2,4);
imagesc(Ichrominance_kmeans);
title('L*a*b chrominance'); axis image; axis off;


%% 
% close all;
% clc;

Isegments = im2double(imread('segments.png'));      

Isegments = Isegments==0;

four_neighborhood = [0, 1, 0;
                     1, 1, 1;
                     0, 1, 0];

eight_neighborhood = [1, 1, 1;
                      1, 1, 1;
                      1, 1, 1];

circle = [1, 1, 1, 1, 1;
          1, 0, 0, 0, 1;
          1, 0, 0, 0, 1;
          1, 0, 0, 0, 1;
          1, 1, 1, 1, 1];
      
cross =  [1, 0, 0, 0, 1;
          0, 1, 0, 1, 0;
          0, 0, 1, 0, 0;
          0, 1, 0, 1, 0;
          1, 0, 0, 0, 1]; 
      
disk = strel('disk', 2);
square = strel('square', 4);
% 6
figure;
subplot(1,2,1);
imagesc(Isegments);
title('Edge detection'); axis image; axis off;

Ifilled = Isegments;
%Ifilled = imdilate(Ifilled, four_neighborhood);
%Ifilled = imerode(Ifilled, eight_neighborhood);
%Ifilled = imdilate(Ifilled, cross);
%Ifilled = imerode(Ifilled, circle);
Ifilled = imdilate(Ifilled, square);
Ifilled = imerode(Ifilled, disk);
Ifilled = imdilate(Ifilled, four_neighborhood);
Ifilled = imerode(Ifilled, eight_neighborhood);

subplot(1,2,2);
imagesc(Ifilled);
title('Fill holes'); axis image; axis off;