
clear all;


close all; clc;

%% Load image
I = imread('plant cell.jpeg');
I = rgb2gray(I);
I_d = im2double(I);

%% Sobel
sobel_fm_u = (1/8) * [1, 0, -1;
                   2, 0, -2;
                   1, 0, -1];
sobel_fm_v = sobel_fm_u';
prewitt_fm_u = (1/6) * [1, 0, -1;
                     1, 0, -1;
                     1, 0, -1];
prewitt_fm_v = prewitt_fm_u';

g_u_sobel = conv2(I_d, sobel_fm_u);
g_v_sobel = conv2(I_d, sobel_fm_v);
g_u_sobel_alt = imfilter(I_d, flip(sobel_fm_u, 2));
g_v_sobel_alt = imfilter(I_d, flip(sobel_fm_v, 1));
% figure 1  y axis
figure;
subplot(3,1,1);
imshow(I_d);
subplot(3,1,2);
imshow(g_u_sobel, [], 'Colormap', cool);
colorbar;
subplot(3,1,3);
imshow(g_u_sobel_alt, [], 'Colormap', cool);
colorbar;
%figure 2   x axis
figure;
subplot(3,1,1);
imshow(I_d);
subplot(3,1,2);
imshow(g_v_sobel, [], 'Colormap', cool);
colorbar;
subplot(3,1,3);
imshow(g_v_sobel_alt, [], 'Colormap', cool);
colorbar;

%% Prewitt
g_u_prewitt = conv2(I_d, prewitt_fm_u);
g_v_prewitt = conv2(I_d, prewitt_fm_v);
 
%% Gradient length and angle
g_sobel_length = sqrt(g_u_sobel.^2 + g_v_sobel.^2);
g_sobel_angle = atan2(g_v_sobel, g_u_sobel);
g_sobel_angle_scaled = (g_sobel_angle / (2 * pi)) + 0.5;
g_prewitt_length = sqrt(g_u_prewitt.^2 + g_v_prewitt.^2);
g_prewitt_angle = atan2(g_v_prewitt, g_u_prewitt);
g_prewitt_angle_scaled = (g_prewitt_angle / (2 * pi)) + 0.5;
% figure 3
figure;
subplot(2,1,1)
imshow(g_sobel_length, [], 'Colormap', jet);
subplot(2,1,2)
imshow(g_prewitt_length, [], 'Colormap', jet);
% figure 4
figure;
subplot(3,1,1);
imshow(I_d);
subplot(3,1,2);
imshow(g_sobel_length, []);
colorbar;
subplot(3,1,3);
imshow(g_prewitt_angle * 180 / pi, [-180, 180], 'Colormap', hsv);
colorbar;
 


%% Laplacian and Sobel
laplacian_fm = fspecial('laplacian');
I_laplacian = conv2(I_d, laplacian_fm);
I_laplacian_bw = im2bw(I_laplacian, 0.15);
I_sobel_bw = im2bw(g_sobel_length, 0.05);
%figure 5
figure;
subplot(2,2,1);
imshow(I_laplacian, []);
colorbar;
subplot(2,2,2);
imshow(g_sobel_length, []);
colorbar;
subplot(2,2,3);
imshow(I_laplacian_bw);
subplot(2,2,4);
imshow(I_sobel_bw);

%% Canny and Laplacian of Gaussian
I_canny = edge(I_d, 'canny', [0.05, 0.14], 2);
I_log = edge(I_d, 'log', 0.0008, 3);
%figure 6
figure;
subplot(2,1,1);
imshow(I_canny);
subplot(2,1,2);
imshow(I_log);
 
%% Hough transform
n_lines = 10;

hough_data = robust_hough(I_canny);
hough_lines = robust_hough_lines(hough_data, n_lines, I_canny);
robust_hough_plot_lines(I_d, hough_lines);
 
[~, index]= sort(hough_data.peaks(:, 3));
peaks = hough_data.peaks(index, :);
peaks = peaks(end - n_lines:end, :);
%figure 7
figure;
imagesc(hough_data.accumulator);
colormap hot;
hold on;
plot(peaks(:,2),peaks(:,1),'wo');
hold off;