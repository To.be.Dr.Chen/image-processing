function [ J ] = fadeout( I, b )
% J = FADEOUT (I, b)
% FADEOUT takes a grey value image I and makes it fade out
% on the boundaries using a trapezoid function. b is the
% maximal distance of pixels from the boundary which are changed
% FADEOUT returns an image J of the same size as I.
I=imread('cell.jpg')
I = rgb2gray(I);
[r,c]=size(I)
b=r/2;
for i=1:b
    f=i/b;
    for j=1:c
        J(i,j)=f*I(i,j);
        J(r+1-i,j)=f*I(r+1-i,j);
    end
end
d=c/2;
for j=1:d
    f=j/d;
    for i=j:r+1-j
        J(i,j)=f*I(i,j);
        J(i,c+1-j)=f*I(i,c+1-j);
    end
end
[x,y]=size(J)
figure
imshow(J)