
clear all; close all; clc;

I = imread('cell.jpg');

figure;
imshow(I);

I_size = size(I)
I_min = min(min(I))
I_max = max(max(I))
I_average = mean(mean(I))
I_median = median(median(I))

gauss_filter = fspecial('gaussian', 51, 4);
I_gauss = imfilter(I, gauss_filter, 'conv');
I_gauss_wiener = deconvwnr(I_gauss, gauss_filter, 0.0001);

motion_blur_filter = fspecial('motion', 15, 0);
I_motion_blur = imfilter(I, motion_blur_filter, 'conv');
I_motion_blur_wiener = deconvwnr(I_motion_blur, motion_blur_filter, 0.01);

I_fadeout = fadeout(I_gauss, 50);
I_fadeout_wiener = deconvwnr(I_fadeout, gauss_filter, 0.0001);
I_pattern = [I, I, I; I, I, I; I, I, I];
I_pattern_gauss = imfilter(I_pattern, gauss_filter, 'conv');
I_pattern_gauss_wiener = deconvwnr(I_pattern_gauss(I_size(1)+1:I_size(1)*2, I_size(2)+1:I_size(2)*2), gauss_filter, 0.0001);

figure;
subplot(2,2,1);
imshow(I_gauss);
subplot(2,2,2);
imshow(I_gauss_wiener);
subplot(2,2,3);
imshow(I_pattern_gauss);
subplot(2,2,4);
imshow(I_pattern_gauss_wiener);

figure;
subplot(3,2,1);
imshow(I_gauss);
subplot(3,2,2);
imshow(I_gauss_wiener);
subplot(3,2,3);
imshow(I_motion_blur);
subplot(3,2,4);
imshow(I_motion_blur_wiener);
subplot(3,2,5);
imshow(I_fadeout);
subplot(3,2,6);
imshow(I_fadeout_wiener);