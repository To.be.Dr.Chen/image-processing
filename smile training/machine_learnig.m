%% Assignment 06
clear all;
close all;
clc;


%% Load dataset
load('smileys_train.mat');
load('smileys_test.mat');

faces.train = train;
faces.trainlabel = trainlabel;
faces.test = test;
faces.testlabel = testlabel;


%% Plot dataset
figure;
for i = 1:6 
    subplot(2, 3, i);
    plot_smiley(faces.train(i, :))
    hold on;
    axis off equal;
end
hold off;


% Train SVM   只有普通的SVM算法
%Train SVM with RBF as kernel function, sigma = 2 and C = 5
svm = fitcsvm(faces.train, faces.trainlabel, ...
              'KernelFunction', 'rbf', ...
              'KernelScale', 2.0, ...
              'BoxConstraint', 5.0);
              
disp('training error: ');
[error, confusion_matrix] = test_svm(svm, faces.train, faces.trainlabel)
disp('validation error: ');
[error, confusion_matrix] = test_svm(svm, faces.test, faces.testlabel)


%% Cross-Validation, k-fold  softmargin，
c_vals = [1:5:25];  %10.^[-5:5];
sigma_vals = [1:5:25]; %10.^[-5:5];

% 3-fold and 10-fold
for k = [3, 10]
    [svm cverror confmat cmin sigmamin] = train_svm_cv (faces.train, faces.trainlabel, k, c_vals, sigma_vals);

    % validate
    disp('training_error whole training:');
    cverror
    confmat
    cmin
    sigmamin
    disp('training_error k-fold:');
    [error, confusionmatrix] = test_svm(svm, faces.train, faces.trainlabel)
    disp('validation_error: ');
    [error, confusionmatrix] = test_svm(svm, faces.test, faces.testlabel)
end

% Run 3-fold/ 10-fold cross validation for single values of sigma and C
% and save the cross-validation error for the contour plot
% Display one plot per value of k
for k = [3, 10]
    score = zeros(length(c_vals), length(sigma_vals));
    i = 1;
    for c = c_vals
        j=1;
        for sigma = sigma_vals        
            [svm cverror confmat cmin sigmamin] = train_svm_cv (faces.train, faces.trainlabel, k, c, sigma);
            score(i,j) = cverror;
            j = j+1;
        end
        i = i+1;
    end

    % Plot
    figure
    contourf(c_vals,sigma_vals,score')
end


%% Evaluating the bottom half of the image
size(faces.train)
size(faces.test)

% Alternative way to remove the bottom half of the image
% bottom_half.train = face.train(:, 201:400);
% bottom_half.test = face.test(:, 201:400);

% Resize and Plot
figure;
bottom_half_temp.train = reshape(faces.train, size(faces.train, 1), 20, 20);
size(bottom_half_temp.train)
for i = [1:size(faces.train, 1)]    
    bhtr = bottom_half_temp.train(i, 1:20, 11:20);
    if i < 7  
        subplot(2, 3, i);
        imagesc(squeeze(bhtr)')
        hold on;
        axis off equal;
    end
    bhtr = reshape(bhtr, 1, []);
    bottom_half.train(i,:) = bhtr;
end
hold off;
size(bottom_half.train)

bottom_half_temp.test = reshape(faces.test, size(faces.test, 1), 20, 20);
size(bottom_half_temp.test)
for i = [1:size(faces.test, 1)]    
    bhte = bottom_half_temp.test(i, 1:20, 11:20);
    bhte = reshape(bhte, 1, []);
    bottom_half.test(i,:) = bhte;
end
size(bottom_half.test)

% Just a copy
bottom_half.trainlabel = faces.trainlabel;
bottom_half.testlabel = faces.testlabel;

% Evaluation
disp('Training and testing, bottom half of the image');
for k = [3, 10]
    [svm cverror confmat cmin sigmamin] = train_svm_cv (bottom_half.train, bottom_half.trainlabel, k, c_vals, sigma_vals);

    % validate
    disp('training_error whole training:');
    cverror
    confmat
    cmin
    sigmamin
    disp('training_error k-fold:');
    [error, confusionmatrix] = test_svm(svm, bottom_half.train, bottom_half.trainlabel)
    disp('validation_error: ');
    [error, confusionmatrix] = test_svm(svm, bottom_half.test, bottom_half.testlabel)
end
% 
% 
%% Evaluating a centred square at the bottom half of the image
% Resize and Plot
figure;
bottom_square_temp.train = reshape(faces.train, size(faces.train, 1), 20, 20);
size(bottom_square_temp.train)
for i = [1:size(faces.train, 1)]    
    bsttr = bottom_square_temp.train(i, 6:15, 11:20);    
    if i < 7
        %smplot(2, 3, i);
        subplot(2, 3, i);
        imagesc(squeeze(bsttr)')
        hold on;
        axis off equal;
    end      
    bsttr = reshape(bsttr, 1, []);
    bottom_square.train(i,:) = bsttr;
end
hold off;
size(bottom_square.train)

bottom_square_temp.test = reshape(faces.test, size(faces.test, 1), 20, 20);
size(bottom_square_temp.test)
for i = [1:size(faces.test, 1)]    
    bstte = bottom_square_temp.test(i, 6:15, 11:20);
    bstte = reshape(bstte, 1, []);
    bottom_square.test(i,:) = bstte;
end
size(bottom_square.test)

% Just a copy
bottom_square.trainlabel = faces.trainlabel;
bottom_square.testlabel = faces.testlabel;

% Evaluation
disp('Training and testing, centered square at the bottom of the image');
for k = [3, 10]
    [svm cverror confmat cmin sigmamin] = train_svm_cv (bottom_square.train, bottom_square.trainlabel, k, c_vals, sigma_vals);

    % validate
    disp('training_error whole training:');
    cverror
    confmat
    cmin
    sigmamin
    disp('training_error k-fold:');
    [error, confusionmatrix] = test_svm(svm, bottom_square.train, bottom_square.trainlabel)
    disp('validation_error: ');
    [error, confusionmatrix] = test_svm(svm, bottom_square.test, bottom_square.testlabel)
end


%% Evaluating mouth area
% Resize and Plot
figure;
mouth_temp.train = reshape(faces.train, size(faces.train, 1), 20, 20);
size(mouth_temp.train)
for i = [1:size(faces.train, 1)]    
    mtr = mouth_temp.train(i, 6:15, 13:18);    
    if i < 7
        subplot(2, 3, i);
        imagesc(squeeze(mtr)')
        hold on;
        axis off equal;
    end      
    mtr = reshape(mtr, 1, []);
    mouth.train(i,:) = mtr;
end
hold off;
size(mouth.train)

mouth_temp.test = reshape(faces.test, size(faces.test, 1), 20, 20);
size(mouth_temp.test)
for i = [1:size(faces.test, 1)]    
    mte = mouth_temp.test(i, 6:15, 13:18);
    mte = reshape(mte, 1, []);
    mouth.test(i,:) = mte;
end
size(mouth.test)

% Just a copy
mouth.trainlabel = faces.trainlabel;
mouth.testlabel = faces.testlabel;

% Evaluation
disp('Training and testing, mouth area');
for k = [3, 10]
    [svm cverror confmat cmin sigmamin] = train_svm_cv (mouth.train, mouth.trainlabel, k, c_vals, sigma_vals);

    % validate
    disp('training_error whole training:');
    cverror
    confmat
    cmin
    sigmamin
    disp('training_error k-fold:');
    [error, confusionmatrix] = test_svm(svm, mouth.train, mouth.trainlabel)
    disp('validation_error: ');
    [error, confusionmatrix] = test_svm(svm, mouth.test, mouth.testlabel)
end
